const InitialState = {
  status: false,
};

export default function(state = InitialState, action) {
  // console.log(action);
  switch (action.type) {
    case 'LOGGED_OUT':
      return {
        status: false,
      };
    case 'LOGGED_IN':
      return {
        status: true,
      };
    default:
      return state;
  }
}
