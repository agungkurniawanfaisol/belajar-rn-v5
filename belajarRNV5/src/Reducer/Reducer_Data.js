const InitialState = {
  data: {},
};

export default function(state = InitialState, action) {
  // console.log(action);
  switch (action.type) {
    case 'UPDATE_DATA':
      return {
        ...state,
        data: action.payload,
      };
    case 'DELETE_DATA':
      return {
        ...state,
        data: {},
      };
    default:
      return state;
  }
}
