import React from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {Action_Login} from '../../Actions/Action_Login';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation, Action_Login_Reducer}) => {
  const klikLogin = async () => {
    try {
      await AsyncStorage.setItem('@storage_Key', 'Setan');
      Action_Login_Reducer(true);
      navigation.navigate('Home');
    } catch (error) {
      console.log('Error klikLogin = ', error);
    }
  };

  return (
    <View>
      <Button onPress={klikLogin} title="Klik" />
    </View>
  );
};

const mapStateToProps = state => {
  // Redux Store --> Component
  return {};
};

// Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
const mapDispatchToProps = dispatch => {
  return {Action_Login_Reducer: status => dispatch(Action_Login(status))};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
