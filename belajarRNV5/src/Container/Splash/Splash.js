import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';

const Splash = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <ActivityIndicator color={'blue'} />
    </View>
  );
};

export default Splash;
