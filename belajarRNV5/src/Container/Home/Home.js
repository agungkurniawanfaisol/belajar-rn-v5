import React from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {Action_Login} from '../../Actions/Action_Login';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({Action_Login_Reducer}) => {
  const PressKeluar = async () => {
    //tes
    try{
      //kalo method clear berati semua data di asyncstorage dihapus, lebih spesifik mungkin lebih direkomendasi 
      await AsyncStorage.removeItem('@storage_Key');
      Action_Login_Reducer(false);
      //setelah ubah state di redux otomatis arah screennya sesuai logic di Router.js
    } catch (e) {
      console.log('Error Press keluar = ', e);
    }
    };

  return (
    <View>
      <Text>Home</Text>
      <Button onPress={PressKeluar} title="Logout" />
    </View>
  );
};
// const mapStateToProps = state => {
//   // Redux Store --> Component
//   return {};
// };

// Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
const mapDispatchToProps = dispatch => {
  return {Action_Login_Reducer: status => dispatch(Action_Login(status))};
};
//alksjdlaskjd

export default connect(
  null,
  mapDispatchToProps,
)(Home);
