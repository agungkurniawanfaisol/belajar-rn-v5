export const Action_Login = status => {
  if(status){
    return {
      type: 'LOGGED_IN'
    }
  } else {
    return {
      type: 'LOGGED_OUT'
    }
  }
};
