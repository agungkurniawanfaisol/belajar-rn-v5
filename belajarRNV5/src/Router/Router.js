import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';
import Login from '../Container/Login/Login';
import Splash from '../Container/Splash/Splash';
import Home from '../Container/Home/Home';
import {Action_Login} from '../Actions/Action_Login';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Router = ({Reducer_Login, Action_Login_Reducer}) => {
  const [StateLoading, setStateLoading] = useState(true);
  const StateLoggedIn = Reducer_Login.status;
  console.log('status login = ', StateLoggedIn);
  // const [StateLoggedIn, setStateLogedIn] = useState(null);

  useEffect(() => {
    const bootstrapAsync = async () => {
      // let userToken;
      try {
        //   console.log(Reducer_Login.state);
        // let K = Object.keys(Data).length;
        // console.log(K);
        // let C = null;
        // if (K == 0) {
        //   C = false;
        // } else {
        //   C = true;
        // }

        // Action_Login_Reducer(C);
        // setStateLogedIn(C);
        //   console.log('Sebelum Eksekusi');
          
        const value = await AsyncStorage.getItem('@storage_Key');
        console.log('value storage = ', value);
        if (!value) {
          Action_Login_Reducer(false);
          // setStateLogedIn(false);
        } else {
          Action_Login_Reducer(true);
          // setStateLogedIn(!StateLoggedIn);
        }
        //biar seakan loading
        setTimeout(() => {
          setStateLoading(false);
        }, 2000);

      } catch (e) {}
    };

    bootstrapAsync();
  }, []);

  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {StateLoading == true ? (
          <Stack.Screen name="Splash" component={Splash} />
        ) : StateLoggedIn == false ? (
          <Stack.Screen name="Login" component={Login} />
        ) : (
          <Stack.Screen name="Home" component={Home} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = state => {
  // Redux Store --> Component
  return {Reducer_Login: state.Reducer_Login};
};

// Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
const mapDispatchToProps = dispatch => {
  return {Action_Login_Reducer: status => dispatch(Action_Login(status))};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Router);
