import * as React from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducer from './../Reducer';
import thunk from 'redux-thunk';
import Router from './Router';

const storeApp = createStore(reducer, applyMiddleware(thunk));

export default function App({navigation}) {
  return (
    <Provider store={storeApp}>
      <Router />
    </Provider>
  );
}
